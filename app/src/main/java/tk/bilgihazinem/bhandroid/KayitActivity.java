
package tk.bilgihazinem.bhandroid;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemFirestore;
import tk.bilgihazinem.bhandroid.model.Kullanici;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class KayitActivity extends BilgiHazinemActivity {

    private HesapYoneticisi mHesapYoneticisi;
    private BilgiHazinemFirestore mFirestore;
    private static final String TAG = "KayitActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kayit);
        mHesapYoneticisi = new HesapYoneticisi(this);
        mFirestore = new BilgiHazinemFirestore(this);
    }
    @Override
    protected void onStart(){
        super.onStart();
        $pb(R.id.progressBar).setVisibility(View.GONE);
    }

    public void kaydol(View view) {
        String email = $et(R.id.act_kayit_et_email).getText().toString();
        String parola = $et(R.id.act_kayit_et_parola).getText().toString();
        $pb(R.id.progressBar).setVisibility(View.VISIBLE);
        mHesapYoneticisi.kullaniciOlustur(email, parola, task -> {
            if(task.isSuccessful()){
                mFirestore.kullaniciEkle(new Kullanici(email));
                // Burada kullanıcı artık giriş yapmıştır. Arayüzü kullanıcıya göre düzenleyebilirsiniz.
                $pb(R.id.progressBar).setVisibility(View.GONE);
                Log.d(TAG, "Kullanıcı başarıyla oluşturuldu.");
                yonlendir(AnasayfaActivity.class);
            } else{
                $pb(R.id.progressBar).setVisibility(View.GONE);
                Log.w(TAG, "Kullanıcı oluştururken bir hata oluştu.", task.getException());
                toast("Kullanıcı oluştururken bir hata oluştu.", Toast.LENGTH_SHORT);
            }
        });
    }

}

