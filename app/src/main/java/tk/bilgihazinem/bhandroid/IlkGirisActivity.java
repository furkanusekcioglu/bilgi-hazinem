package tk.bilgihazinem.bhandroid;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

import java.lang.ref.WeakReference;

public class IlkGirisActivity extends BilgiHazinemActivity {
    private static final String TAG = "IlkGirisActivity";
    private BilgiHazinemSqlite mSqLite;
    private HesapYoneticisi mHy;
    private Context ctx;
    private BilgiHazinemActivity bha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilk_giris);
        mHy = new HesapYoneticisi(this);
        mSqLite = new BilgiHazinemSqlite(this);
        mSqLite.tablolariOlustur();
        tiklamaDinleyicileriniAta();
    }
    private void tiklamaDinleyicileriniAta(){
        // Sandık oluştur butonu
        $btn(R.id.act_ilkGiris_btn_sandikOlustur).setOnClickListener(v ->{
            yonlendir(SandikOlusturActivity.class);
        });
        // Örnek sandıkları getir butonu
        $btn(R.id.act_ilkGiris_btn_ornekSandiklariGetir).setOnClickListener(v ->{
            new OrnekSandiklariGetirTask(this, mSqLite, mHy).execute();
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(mHy.yerelHesapVarMi()){
            yonlendir(AnasayfaActivity.class);
        }
        $pb(R.id.progressBar).setVisibility(View.GONE); //progress bar'ı göster.
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mHy.yerelHesapVarMi()){
            yonlendir(AnasayfaActivity.class);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSqLite.db.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private static class OrnekSandiklariGetirTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<IlkGirisActivity> mActivityReference;
        private BilgiHazinemSqlite mSqLite;
        private HesapYoneticisi mHy;

        protected void onPreExecute() {
            IlkGirisActivity activity = mActivityReference.get();
            activity.$pb(R.id.progressBar).setVisibility(View.VISIBLE); //progress bar'ı göster.
        }

        // only retain a weak reference to the activity
        OrnekSandiklariGetirTask(IlkGirisActivity context, BilgiHazinemSqlite sqLite, HesapYoneticisi hy) {
            mActivityReference = new WeakReference<>(context);
            mSqLite = sqLite;
            mHy = hy;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            mSqLite.tablolariOlustur();
            mSqLite.ornekVerileriEkle();
            mHy.yerelHesapOlustur();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            IlkGirisActivity activity = mActivityReference.get();
            activity.$pb(R.id.progressBar).setVisibility(View.GONE);
            Intent intent = new Intent(activity.getApplicationContext(), AnasayfaActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }

}
