package tk.bilgihazinem.bhandroid;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemFirestore;
import tk.bilgihazinem.bhandroid.model.Bilgi;
import tk.bilgihazinem.bhandroid.model.Sandik;

public class OturumAcActivity extends BilgiHazinemActivity {

    private HesapYoneticisi mHy;
    private BilgiHazinemFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oturum_ac);
        getSupportActionBar().setTitle("Oturum Aç");
        mHy = new HesapYoneticisi(this);
        mFirestore = new BilgiHazinemFirestore(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        $pb(R.id.progressBar).setVisibility(View.GONE);
    }

    public void oturumAc(View view) {
        String email = $et(R.id.act_oturum_ac_et_email).getText().toString();
        String parola = $et(R.id.act_oturum_ac_et_parola).getText().toString();
        $pb(R.id.progressBar).setVisibility(View.VISIBLE);
        mHy.girisYap(email, parola, task -> {
            if(task.isSuccessful()){
                $pb(R.id.progressBar).setVisibility(View.GONE);
                toast("Hoşgeldin " + mHy.kullaniciEmailiniAl(), Toast.LENGTH_SHORT);
                yonlendir(AnasayfaActivity.class);
            } else{
                $pb(R.id.progressBar).setVisibility(View.GONE);
                toast("Giriş yaparken bir hata oluştu.", Toast.LENGTH_SHORT);
            }
        });
    }
}

