package tk.bilgihazinem.bhandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.widget.EditText;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

public class SandikDuzenleActivity extends BilgiHazinemActivity {

    private BilgiHazinemSqlite mSqLite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandik_duzenle);
        mSqLite = new BilgiHazinemSqlite(this);
        tiklamaDinleyicileriniAta();
        Bundle extras = getIntent().getExtras();

        EditText sandikAdiEdtTxt = findViewById(R.id.act_sandik_duzenle_et_sandik_adi);
        sandikAdiEdtTxt.setText(extras.getString("sandikAdi"));
        sandikAdiEdtTxt.setSelection(sandikAdiEdtTxt.getText().length());
        showKeyboard(sandikAdiEdtTxt);
    }


    private void tiklamaDinleyicileriniAta() {
        // Sandik Duzenle Butonu
        $btn(R.id.act_sandik_duzenle_btn_sandik_duzenle).setOnClickListener(v -> {
            //Sandığı sqlite'a ekle.
            String yeniSandikAdi = $et(R.id.act_sandik_duzenle_et_sandik_adi).getText().toString();
            Bundle extras = getIntent().getExtras();
            if(mSqLite.sandikDuzenle(extras.getLong("sandikId"), yeniSandikAdi)){
                toast("Sandık başarıyla güncellendi.");
                Intent i = new Intent(SandikDuzenleActivity.this, AnasayfaActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("sandikAdi", yeniSandikAdi);
                startActivity(i);
                finish();
            }
        });

        // Sandik Sil Butonu
        $btn(R.id.act_sandik_duzenle_btn_sandik_sil).setOnClickListener(v -> {
            String sandikAdi = getIntent().getExtras().getString("sandikAdi");
            kullaniciOnayi(sandikAdi); //AlertDialog gösterir. Silme işlemi içersinde gerçekleşiyor.
        });


    }

    private void kullaniciOnayi(String sandikAdi){
        // https://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // android.R.style.Theme_Material_Dialog_Alert kısmı normalde hata veriyordu.
            // context parametresine getApplicationContext() yerine this yazınca düzeldi.
            // https://stackoverflow.com/questions/21814825/you-need-to-use-a-theme-appcompat-theme-or-descendant-with-this-activity
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("\"" + sandikAdi + "\"" + " Silinecek")
                .setMessage("Bilgi sandığınız kaldırılacak. Onaylıyor musunuz?")
                .setPositiveButton(R.string.evet, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sandigiSil();
                    }
                })
                .setNegativeButton(R.string.hayir, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // bir şey yapma
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void sandigiSil(){
        Long sandikId = getIntent().getExtras().getLong("sandikId");
        if(mSqLite.sandikSil(sandikId)){
            toast("Sandık başarıyla silindi.");
            Intent i = new Intent(SandikDuzenleActivity.this, AnasayfaActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }
}

