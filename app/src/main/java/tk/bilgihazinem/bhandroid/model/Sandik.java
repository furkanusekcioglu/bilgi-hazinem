package tk.bilgihazinem.bhandroid.model;

public class Sandik {
    private Long mId;
    private String mSandikAdi;
    public Sandik(Long id, String sandikAdi){
        mId = id;
        mSandikAdi = sandikAdi;
    }
    public Sandik(){}

    public String getSandikAdi(){
        return mSandikAdi;
    }

    public Long getId(){
        return mId;
    }

    public void setSandikAdi(String sandikAdi){
        mSandikAdi = sandikAdi;
    }
    public void setId(Long id){
        mId = id;
    }

}
