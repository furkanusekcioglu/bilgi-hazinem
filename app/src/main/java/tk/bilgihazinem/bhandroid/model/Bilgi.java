package tk.bilgihazinem.bhandroid.model;

public class Bilgi {
    private Long mId;
    private String mBilgi;
    private String mAciklama;

    public Bilgi(Long id, String bilgi, String aciklama) {
        mId = id;
        mBilgi = bilgi;
        mAciklama = aciklama;
    }
    public Bilgi() {}

    public Long getId() {
        return mId;
    }
    public void setId(Long id) {
        mId = id;
    }
    public String getBilgi() {
        return mBilgi;
    }
    public void setBilgi(String bilgi) {
        mBilgi = bilgi;
    }
    public String getAciklama() {
        return mAciklama;
    }
    public void setAciklama(String aciklama) {
        mAciklama = aciklama;
    }
}

