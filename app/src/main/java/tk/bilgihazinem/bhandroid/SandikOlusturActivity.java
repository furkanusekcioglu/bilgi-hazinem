package tk.bilgihazinem.bhandroid;

import android.content.Intent;
import android.os.Bundle;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemFirestore;
import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemFirestore;
import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

public class SandikOlusturActivity extends BilgiHazinemActivity {

    private BilgiHazinemFirestore mFirestore;
    private BilgiHazinemSqlite mSqLite;
    private HesapYoneticisi mHesapYoneticisi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandik_olustur);

        mHesapYoneticisi = new HesapYoneticisi(this);
        mFirestore = new BilgiHazinemFirestore(this);
        mSqLite = new BilgiHazinemSqlite(this);
        tiklamaDinleyicileriniAta();
    }

    private void tiklamaDinleyicileriniAta() {
        // Sandik Oluştur Butonu
        $btn(R.id.act_sandikOlustur_btn_sandikOlustur).setOnClickListener(v -> {
            //Sandığı sqlite'a ekle.
            String sandikAdi = $et(R.id.act_sandikOlustur_et_sandikAdi).getText().toString();
            mSqLite.sandikEkle(sandikAdi);
            mHesapYoneticisi.yerelHesapOlustur();
            // Anasayfaya dön
            Intent i = new Intent(SandikOlusturActivity.this, AnasayfaActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("sandikAdi", sandikAdi);
            startActivity(i);
        });
    }

}

