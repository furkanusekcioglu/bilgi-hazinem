package tk.bilgihazinem.bhandroid;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class HesapYoneticisi {
    private BilgiHazinemActivity mActivity;
    private FirebaseAuth mAuth;

    public HesapYoneticisi(BilgiHazinemActivity activity){
        mActivity = activity;
        mAuth = FirebaseAuth.getInstance();
    }
    public void oturumuKapat(){
        FirebaseAuth.getInstance().signOut();
        mActivity.deleteDatabase("BilgiHazinem");
        yerelHesabiSil();
        mActivity.yonlendir(AnasayfaActivity.class);
    }
    public void verileriSil(){
        mActivity.deleteDatabase("BilgiHazinem");
        yerelHesabiSil();
    }
    public Boolean girisYapildiMi(){
        return mAuth.getCurrentUser() != null;
    }
    public void kullaniciOlustur(String eposta, String parola, OnCompleteListener<AuthResult> onCompleteListener){
        mAuth.createUserWithEmailAndPassword(eposta, parola).addOnCompleteListener(onCompleteListener);
    }
    public void girisYap(String eposta, String parola, OnCompleteListener<AuthResult> onCompleteListener){
        mAuth.signInWithEmailAndPassword(eposta, parola).addOnCompleteListener(onCompleteListener);
    }
    public void yerelHesapOlustur(){
        SharedPreferences sharedPreferences = mActivity.getSharedPreferences("YerelHesapPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("YerelHesapVar", true);
        editor.commit();
    }
    public void yerelHesabiSil(){
        SharedPreferences sharedPreferences = mActivity.getSharedPreferences("YerelHesapPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("YerelHesapVar", false);
        editor.commit();
    }
    public boolean yerelHesapVarMi(){
        SharedPreferences sharedPreferences = mActivity.getSharedPreferences("YerelHesapPreferences", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("YerelHesapVar", false);
    }
    public String kullaniciEmailiniAl(){
        return mAuth.getCurrentUser().getEmail();
    }
}

