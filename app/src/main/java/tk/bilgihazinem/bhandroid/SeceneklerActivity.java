package tk.bilgihazinem.bhandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.lang.ref.WeakReference;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemFirestore;

public class SeceneklerActivity extends BilgiHazinemActivity {

    private HesapYoneticisi mHy;
    private FirebaseAuth mAuth;
    private BilgiHazinemFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secenekler);

        mAuth = FirebaseAuth.getInstance();
        mHy = new HesapYoneticisi(this);
        mFirestore = new BilgiHazinemFirestore(this);
        butonTiklamaDinleyicileriniAta();
        if (!mHy.girisYapildiMi()){
            $btn(R.id.act_secenekler_btn_verileri_yedekle).setVisibility(View.GONE);
            $btn(R.id.act_secenekler_btn_yedeklenmis_verileri_al).setVisibility(View.GONE);
        }
    }
    public void onStart(){
        super.onStart();
        $pb(R.id.progressBar).setVisibility(View.GONE);
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            //$tv(R.id.act_secenekler_et_profilolustur).setVisibility(View.GONE);
            $btn(R.id.act_secenekler_btn_profilOlustur).setVisibility(View.GONE);
            $btn(R.id.act_secenekler_btn_oturum).setText("Oturumu Kapat");
        }
    }
    public void profilOlusturTikla(View v){
        yonlendir(KayitActivity.class);
    }
    private static class VerileriSilTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<SeceneklerActivity> mActivityReference;
        private HesapYoneticisi mHy;

        protected void onPreExecute() {
            SeceneklerActivity activity = mActivityReference.get();
            activity.$pb(R.id.progressBar).setVisibility(View.VISIBLE); //progress bar'ı göster.
        }

        // only retain a weak reference to the activity
        VerileriSilTask(SeceneklerActivity context, HesapYoneticisi hy) {
            mActivityReference = new WeakReference<>(context);
            mHy = hy;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            mHy.verileriSil();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            SeceneklerActivity activity = mActivityReference.get();
            activity.$pb(R.id.progressBar).setVisibility(View.GONE);
            Intent intent = new Intent(activity.getApplicationContext(), IlkGirisActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }
    private void butonTiklamaDinleyicileriniAta(){
        $btn(R.id.act_secenekler_btn_verileriSil).setOnClickListener(v -> {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // android.R.style.Theme_Material_Dialog_Alert kısmı normalde hata veriyordu.
                // context parametresine getApplicationContext() yerine this yazınca düzeldi.
                // https://stackoverflow.com/questions/21814825/you-need-to-use-a-theme-appcompat-theme-or-descendant-with-this-activity
                builder = new AlertDialog.Builder(SeceneklerActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(SeceneklerActivity.this);
            }
            builder.setTitle("Bütün verileriniz silinecek")
                    .setMessage("Size ait bütün veriler kaldırılacak. Bu işlem geri alınamaz. Onaylıyor musunuz?")
                    .setPositiveButton(R.string.evet, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new VerileriSilTask(SeceneklerActivity.this, mHy).execute();
                        }
                    })
                    .setNegativeButton(R.string.hayir, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // bir şey yapma
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });
        $btn(R.id.act_secenekler_btn_oturum).setOnClickListener(v ->{
            if (mAuth.getCurrentUser() != null){
                FirebaseAuth.getInstance().signOut();
                toast("Oturum kapatıldı.");
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
            else {
                yonlendir(OturumAcActivity.class);
            }
        });
        $btn(R.id.act_secenekler_btn_verileri_yedekle).setOnClickListener(v -> {
            $pb(R.id.progressBar).setVisibility(View.VISIBLE);
            mFirestore.verileriYedekle(mHy.kullaniciEmailiniAl());
//            toast("Veriler başarıyla bulutta yedeklendi.");
        });
        $btn(R.id.act_secenekler_btn_yedeklenmis_verileri_al).setOnClickListener(v -> {
            mFirestore.yedeklenmisVerileriAl(mHy.kullaniciEmailiniAl());
            toast("Veriler buluttan alındı.");
        });
    }
}
/* * * * HATA VAR -- progress bar genel bir layouta alınabilir* * * */
// TODO PROGRESSBAR GENEL BİR LAYOUTA ALINACAK YA DA HER SAYFAYA EKLENECEK


