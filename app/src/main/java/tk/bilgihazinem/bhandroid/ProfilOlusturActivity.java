package tk.bilgihazinem.bhandroid;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfilOlusturActivity extends BilgiHazinemActivity {
    Button hesapOlustur;
    EditText email;
    EditText parola;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_olustur);

        getSupportActionBar().setTitle("Profil Oluştur");

        mAuth = FirebaseAuth.getInstance();

        hesapOlustur = findViewById(R.id.act_profilOlustur_btn_hesapOlustur);
        email = findViewById(R.id.act_profilOlustur_et_email);
        parola = findViewById(R.id.act_profilOlustur_et_parola);
    }
    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            Toast.makeText(ProfilOlusturActivity.this, "Anasayfaya yönlendiriliyorsunuz.", Toast.LENGTH_SHORT).show();
            yonlendir(AnasayfaActivity.class);
        }
    }

    public void profilOlusturTikla(View view){
        mAuth.createUserWithEmailAndPassword(email.getText().toString(),parola.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(ProfilOlusturActivity.this, "Kullanıcı oluşturuldu", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user!=null){
                                Toast.makeText(ProfilOlusturActivity.this, "Anasayfaya yönlendiriliyorsunuz.", Toast.LENGTH_SHORT).show();
                                yonlendir(AnasayfaActivity.class);
                            }
                        }

                    }
                }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ProfilOlusturActivity.this, "Bir hata gerçekleşti. Kullanıcı oluşturulamadı.", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
