package tk.bilgihazinem.bhandroid;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

public class BilgiDuzenleActivity extends BilgiHazinemActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bilgi_duzenle);
        Bundle extras = getIntent().getExtras();

        EditText bilgiEdtTxt = findViewById(R.id.act_bilgi_duzenle_et_bilgi);

        String bilgi = extras.getString("bilgi");
        String aciklama = extras.getString("aciklama");
        bilgiEdtTxt.setText(bilgi);
        $et(R.id.act_bilgi_duzenle_et_aciklama).setText(aciklama);


        bilgiEdtTxt.setSelection(bilgiEdtTxt.getText().length());
        showKeyboard(bilgiEdtTxt);

        butonDinleyicileriniAta();
    }

    private void butonDinleyicileriniAta(){
        $btn(R.id.act_bilgi_duzenle_btn_kaydet).setOnClickListener(v -> {
            Bundle extras = getIntent().getExtras();
            Long sandikId = extras.getLong("sandikId");
            String bilgi = extras.getString("bilgi");
            String sandikAdi = extras.getString("sandikAdi");

            String yeniBilgi = $tv(R.id.act_bilgi_duzenle_et_bilgi).getText().toString();
            String yeniAciklama = $tv(R.id.act_bilgi_duzenle_et_aciklama).getText().toString();
            if (yeniBilgi.isEmpty()){
                toast("Bilgi boş olamaz.", Toast.LENGTH_SHORT);
            }
            else{
                BilgiHazinemSqlite sqLite = new BilgiHazinemSqlite(this);
                if(sqLite.bilgiDuzenle(sandikId, bilgi, yeniBilgi, yeniAciklama)){
                    toast("Bilgi başarıyla düzenlendi.", Toast.LENGTH_SHORT);
                    Intent intent = new Intent(BilgiDuzenleActivity.this, AnasayfaActivity.class);
                    intent.putExtra("sandikAdi", sandikAdi);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }
}
