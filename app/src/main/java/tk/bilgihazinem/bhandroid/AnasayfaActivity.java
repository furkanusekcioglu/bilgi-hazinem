package tk.bilgihazinem.bhandroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDoneException;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import tk.bilgihazinem.bhandroid.adapter.BilgilerRecyclerViewAdapter;
import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;
import tk.bilgihazinem.bhandroid.model.Sandik;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class AnasayfaActivity extends BilgiHazinemActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BilgilerRecyclerViewAdapter.ItemClickListener {
    private static final String TAG = "AnasayfaActivity";
    private ArrayList<String> mBilgiler;
    private Sandik mSuankiSandik;
    private int mSeciliBilgiPozisyonu;
    private BilgiHazinemSqlite mSqLite;
    private BilgilerRecyclerViewAdapter mAdapter;
    private RecyclerView mBilgilerRecycler;
    private FirebaseAuth mAuth;
    private HesapYoneticisi mHy;
    private NavigationView mNavigationView;
    private static final String VARSAYILAN_BILGI_ACIKLAMASI = "Bir şey seçmediniz veya açıklama yok.";
    private BottomNavigationView.OnNavigationItemSelectedListener mOnBottomNavigationItemSelectedListener = item ->  {
        // TODO burayı lambasız hale çevirip öyle dene.
        // Burası yerine oncreate içinde yazıp öyle dene.
        item.setChecked(false);
        switch (item.getItemId()) {
            case R.id.navigation_bilgi_ekle:
                NavigationView navView = $(R.id.nav_view);
                Menu navMenu = navView.getMenu();
                int sandikId = -1;
                String sandikAdi = null;
                for(int i = 0; i < navMenu.size(); i++){
                    MenuItem menuItem = navMenu.getItem(i);
                    if(menuItem.isChecked()){
                        sandikId = menuItem.getItemId();
                        sandikAdi = menuItem.getTitle().toString();
                        break;
                    }
                }
                Intent intent = new Intent(AnasayfaActivity.this, BilgiEkleActivity.class);
                intent.putExtra("sandikId", sandikId);
                intent.putExtra("sandikAdi", sandikAdi);
                startActivity(intent);
                return true;


            case R.id.navigation_secenekler:
                Intent intentSecenekler = new Intent(AnasayfaActivity.this, SeceneklerActivity.class);
                startActivity(intentSecenekler);
                break;

            case R.id.navigation_ara:
                $et(R.id.act_anasayfa_et_ara).setSelection($et(R.id.act_anasayfa_et_ara).getText().length());
                showKeyboard($et(R.id.act_anasayfa_et_ara));
                break;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: AnasayfaActivity oluşturuldu.");
        super.onCreate(savedInstanceState);
        // Hazır gelenler
        setContentView(R.layout.activity_anasayfa);
        Toolbar toolbar = $(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = $(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = $(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        // Bizim yazdıklarımız
        firebaseAuthIlkle();
        mSqLite = new BilgiHazinemSqlite(this);
        mHy = new HesapYoneticisi(this);

        // TextView kullanarak RecyclerView'de arama ve filtreleme yapma
        // https://www.youtube.com/watch?v=OWwOSLfWboY
        $et(R.id.act_anasayfa_et_ara).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                s.toString();
                ArrayList<String> filtrelenmisBilgiler = filtrelenmisBilgileriAl(s.toString());
                mAdapter.filterList(filtrelenmisBilgiler);
                if(filtrelenmisBilgiler.size() > 0){
                    String bilgi = filtrelenmisBilgiler.get(0);
                    String bilgiAciklamasi = mSqLite.bilgiAciklamasiniAl(mSuankiSandik.getId(), bilgi);
                    $tv(R.id.act_anasayfa_tv_bilgi_baslik).setText(bilgi);
                    $tr(R.id.row_anasayfa_baslik).setVisibility(View.VISIBLE);
                    $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText(bilgiAciklamasi);
                    ImageButton btnDuzenle = $(R.id.act_anasayfa_btn_duzenle);
                    btnDuzenle.setVisibility(View.VISIBLE);
                } else {
                    $tr(R.id.row_anasayfa_baslik).setVisibility(View.GONE);
                    ImageButton btnDuzenle = $(R.id.act_anasayfa_btn_duzenle);
                    btnDuzenle.setVisibility(View.GONE);
                    $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText(VARSAYILAN_BILGI_ACIKLAMASI);
                }
            }
        });
        if (yonlendirmeKontrolu()) {
            butonTiklamaDinleyicileriniAta();
            mSuankiSandik = new Sandik();
            mSeciliBilgiPozisyonu = -1;
            recyclerViewIlkle();
            //Bilgileri doldur
            Bundle extras = getIntent().getExtras();
            if (extras != null)
                mSuankiSandik.setSandikAdi(extras.getString("sandikAdi", mSqLite.ilkSandikAdiniAl()));
            else
                mSuankiSandik.setSandikAdi(mSqLite.ilkSandikAdiniAl());
            mSuankiSandik.setId(mSqLite.sandikIdsiniAl(mSuankiSandik.getSandikAdi()));
            bilgileriDoldur(mSuankiSandik.getSandikAdi());
            getSupportActionBar().setTitle(mSuankiSandik.getSandikAdi()); //Başlığı değiştir.

            BottomNavigationView bottomNavigation = $(R.id.bottom_navigation);
            bottomNavigation.setOnNavigationItemSelectedListener(mOnBottomNavigationItemSelectedListener);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        $pb(R.id.progressBar).setVisibility(View.GONE);
        mNavigationView.getMenu().removeGroup(R.id.nav_group_sandiklar);
        navViewDoldur();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = $(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.anasayfa, menu);
        return true;
    }
    @Override
    // Sağ üstteki 3 noktadaki bir elemana tıklayınca
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_sandigi_duzenle){
            sandikDuzenleyeYonlendir();
        }
        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    // Soldan açılan navigasyonun elemanlarına tıklanınca
    public boolean onNavigationItemSelected(MenuItem item) {
        for (int i = 0; i < mNavigationView.getMenu().size(); i++)
            mNavigationView.getMenu().getItem(i).setChecked(false);
        item.setChecked(true);
        int id = item.getItemId();
        if (id == R.id.nav_sandik_ekle){
            yonlendir(SandikOlusturActivity.class);
            return false;
        }
        else{
            String sandikAdi = item.getTitle().toString();
            bilgileriDoldur(sandikAdi);

            DrawerLayout drawer = $(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            mSuankiSandik.setId((long)item.getItemId());
            mSuankiSandik.setSandikAdi(item.getTitle().toString());
            baslikAta(item.getTitle().toString());
            return true;
        }

    }
    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy: AnasayfaActivity yok edildi.");
        mSqLite.db.close();
        super.onDestroy();
    }

    private void butonTiklamaDinleyicileriniAta(){
        // Düzenle Butonu tıklama olayı
        ImageButton btnDuzenle = $(R.id.act_anasayfa_btn_duzenle);
        btnDuzenle.setOnClickListener(v -> {
            if(mSeciliBilgiPozisyonu != -1){
                Intent intent = new Intent(AnasayfaActivity.this, BilgiDuzenleActivity.class);
                intent.putExtra("sandikId", mSuankiSandik.getId());
                intent.putExtra("sandikAdi", mSuankiSandik.getSandikAdi());
                intent.putExtra("bilgi", mAdapter.getItem(mSeciliBilgiPozisyonu));
                intent.putExtra("aciklama", $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).getText().toString());
                startActivity(intent);
            }else{
                toast("Bilgi seçmediniz.");
            }
        });
    }
    private void bilgileriDoldur(String sandikAdi){
        String sql = "select * from Sandiklar s, Bilgiler b " +
                "where s.sandik_adi=? and b.sandik_id = s.id";
        String[] selectionArgs = {sandikAdi};
        Cursor cr = mSqLite.db.rawQuery(sql, selectionArgs);
        mBilgiler = new ArrayList<>();

        try{
            while(cr.moveToNext()){
                String bilgi = cr.getString(cr.getColumnIndex("bilgi"));
                mBilgiler.add(bilgi);
            }
        } finally {
            cr.close();
        }
        mAdapter = new BilgilerRecyclerViewAdapter(this, mBilgiler);
        mAdapter.setClickListener(this);
        RecyclerView recyclerView = $(R.id.rv_bilgiler);
        recyclerView.setAdapter(mAdapter);
        $tr(R.id.row_anasayfa_baslik).setVisibility(View.GONE);
        $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText(VARSAYILAN_BILGI_ACIKLAMASI);
    }
    @Override
    // Bilgilerden birine tıklanınca çalışır
    public void onItemClick(View view, int position) {
        try {
            String aciklama = mSqLite.bilgiAciklamasiniAl(mSuankiSandik.getId(), mAdapter.getItem(position));
            $tr(R.id.row_anasayfa_baslik).setVisibility(View.VISIBLE);
            $tv(R.id.act_anasayfa_tv_bilgi_baslik).setText(mAdapter.getItem(position));
            $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText(aciklama); // Açıklamayı textview'de göster.
        } catch(SQLiteDoneException e){ //muhtemelen açıklama bulunamadı.
            $tr(R.id.row_anasayfa_baslik).setVisibility(View.GONE);

            $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText(VARSAYILAN_BILGI_ACIKLAMASI);
        } finally {
            mSeciliBilgiPozisyonu = position;
        }
    }
    private void recyclerViewIlkle(){
        mBilgilerRecycler = findViewById(R.id.rv_bilgiler);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mBilgilerRecycler.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mBilgilerRecycler.getContext(),
                layoutManager.getOrientation());
        mBilgilerRecycler.addItemDecoration(dividerItemDecoration);
        mBilgilerRecycler.setLayoutManager(layoutManager);
    }
    private void firebaseAuthIlkle(){
        mAuth = FirebaseAuth.getInstance();
    }
    private boolean yonlendirmeKontrolu(){
        if (!mHy.yerelHesapVarMi()){
            yonlendir(IlkGirisActivity.class);
            finish();
            return false; //TODO belki silinebilir.
        }
        else if(mSqLite.sandikSayisiniAl() == 0){
            $tr(R.id.row_anasayfa_baslik).setVisibility(View.GONE);
            $tv(R.id.act_anasayfa_tv_bilgi_aciklamasi).setText("Hiç sandığınız yok. Sandık eklemek için sol üstteki butona tıklayın. Örnek sandıkları tekrar görmek için Seçenekler/Verileri Sil yönergesini izleyin.");
            return false;
        }
        return true;
    }
    private void navViewDoldur(){
        Cursor sandiklar = mSqLite.tumSandiklariAl();
        final Menu menu = mNavigationView.getMenu();
        try{
            while(sandiklar.moveToNext()){
                String sandikAdi = sandiklar.getString(sandiklar.getColumnIndex("sandik_adi"));
                int id = sandiklar.getInt(sandiklar.getColumnIndex("id"));
                MenuItem item =  menu.add(R.id.nav_group_sandiklar, id, 0, sandikAdi);
                item.setIcon(R.drawable.ic_menu_send);
                if(sandikAdi.equals(mSuankiSandik.getSandikAdi())){
                    item.setChecked(true);
                }
            }
        } finally {
            sandiklar.close();
        }

    }
    private void sandikDuzenleyeYonlendir(){
        Intent i = new Intent(AnasayfaActivity.this, SandikDuzenleActivity.class);
        i.putExtra("sandikAdi", mSuankiSandik.getSandikAdi());
        i.putExtra("sandikId", mSuankiSandik.getId());
        startActivity(i);
    }
    /*   text: arama çubuğunda yazılan text
     * */
    private ArrayList<String> filtrelenmisBilgileriAl(String text){
        if (!text.isEmpty()){
            ArrayList<String> filteredList = new ArrayList<>();
            for(String bilgi : mBilgiler){
                if(bilgi.toLowerCase().startsWith(text.toLowerCase()))
                    filteredList.add(bilgi);
            }
            return filteredList;
        } else
            return mBilgiler;
    }
    private void baslikAta(String baslikAta){
        getSupportActionBar().setTitle(baslikAta);
    }
}

