
package tk.bilgihazinem.bhandroid;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import tk.bilgihazinem.bhandroid.dal.BilgiHazinemSqlite;

public class BilgiEkleActivity extends BilgiHazinemActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bilgi_ekle);

        showKeyboard($et(R.id.act_bilgi_ekle_et_bilgi));
        $btn(R.id.act_bilgi_ekle_btn_kaydet).setOnClickListener(v -> {
            Bundle extras = getIntent().getExtras();
            int sandikId = extras.getInt("sandikId");
            String sandikAdi = extras.getString("sandikAdi");
            String bilgi = $tv(R.id.act_bilgi_ekle_et_bilgi).getText().toString();
            if (bilgi.isEmpty()){
                toast("Bilgi boş olamaz.", Toast.LENGTH_SHORT);
            }
            else {
                String aciklama = $tv(R.id.act_bilgi_ekle_et_aciklama).getText().toString();
                BilgiHazinemSqlite sqLite = new BilgiHazinemSqlite(this);
                if(sqLite.bilgiEkle(Long.valueOf(sandikId), bilgi, aciklama)){
                    toast("Bilgi başarıyla eklendi.",Toast.LENGTH_SHORT);
                    Intent intent = new Intent(BilgiEkleActivity.this, AnasayfaActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("sandikAdi", sandikAdi);
                    startActivity(intent);
                }
            }
        });
    }
}

