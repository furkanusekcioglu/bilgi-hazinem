package tk.bilgihazinem.bhandroid.dal;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import tk.bilgihazinem.bhandroid.BilgiHazinemActivity;
import tk.bilgihazinem.bhandroid.R;

import java.util.Calendar;
import java.util.Scanner;

import tk.bilgihazinem.bhandroid.BilgiHazinemActivity;

import static android.content.Context.MODE_PRIVATE;

public class BilgiHazinemSqlite {
    public SQLiteDatabase db;
    private BilgiHazinemActivity mActivity;

    public BilgiHazinemSqlite(BilgiHazinemActivity activity){
        mActivity = activity;
        db = mActivity.openOrCreateDatabase("BilgiHazinem", MODE_PRIVATE, null);
    }
    public void tablolariOlustur(){
        sandiklarTablosunuOlustur();
        bilgilerTablosunuOlustur();
    }
    public void ornekVerileriEkle(){
        ornekSandiklariEkle();
        ornekBilgileriEkle();
    }
    public void tablolariSil(){
        sandiklarTablosunuSil();
        bilgilerTablosunuSil();
    }
    public boolean tabloVarMi(String tabloAdi){
        String sql = "select count(*) from sqlite_master where tbl_name = ?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.bindString(1, tabloAdi);
        long result = stmt.simpleQueryForLong();
        return result > 0 ? true : false;
    }
    public void tablolariBosalt(){
        tablolariSil();
        tablolariOlustur();
    }


    // ----------- SANDIKLAR -----------
    //Sandık sorgular
    public String ilkSandikAdiniAl(){
        String sql = "select sandik_adi from Sandiklar limit 1";
        SQLiteStatement stmt = db.compileStatement(sql);
        return stmt.simpleQueryForString();
    }
    public long sandikSayisiniAl(){
        String sql = "select count(*) from Sandiklar";
        return db.compileStatement(sql).simpleQueryForLong();
    }
    public Cursor tumSandiklariAl(){
        String tablo = "Sandiklar";
        String[] dondurulecekKolonlar = {"id", "sandik_adi"};
        Cursor sandiklar = db.query(tablo, dondurulecekKolonlar, null, null,
                null, null, null);
        return sandiklar;
    }
    public Long sandikIdsiniAl(String sandikAdi){
        String sql = "select id from Sandiklar where sandik_adi=?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.bindString(1, sandikAdi);
        return stmt.simpleQueryForLong();
    }
    public boolean sandikVarMi(String sandikAdi){
        String sql = "select count(*) from Sandiklar where sandikAdi = ?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.bindString(1, sandikAdi);
        long result = stmt.simpleQueryForLong();
        return result > 0 ? true : false;
    }
    //Sandık ekleme/düzenleme/silme
    public boolean sandikEkle(String sandikAdi){
        // Insert statement oluştur.
        String insertSql = "insert into Sandiklar (sandik_adi, olusturulma_tarihi) values (?, ?)";
        SQLiteStatement insertStmt = db.compileStatement(insertSql);

        try {
            insertStmt.bindString(1, sandikAdi);
            insertStmt.bindString(2, Calendar.getInstance().getTime().toString());
            insertStmt.executeInsert();
            return true;
        }
        catch(SQLiteConstraintException e) { // Muhtemelen aynı kaydı tekrar eklemeye çalışıyor.
            return false;
        }
    }
    public boolean sandikEkle(Long sandikId, String sandikAdi){
        // Insert statement oluştur.
        String insertSql = "insert into Sandiklar (id, sandik_adi, olusturulma_tarihi) values (?, ?, ?)";
        SQLiteStatement insertStmt = db.compileStatement(insertSql);

        try {
            insertStmt.bindLong(1, sandikId);
            insertStmt.bindString(2, sandikAdi);
            insertStmt.bindString(3, Calendar.getInstance().getTime().toString());
            insertStmt.executeInsert();
            return true;
        }
        catch(SQLiteConstraintException e) { // Muhtemelen aynı kaydı tekrar eklemeye çalışıyor.
            return false;
        }
    }


    public boolean sandikDuzenle(Long sandikId, String yeniSandikAdi){
        try{
            String sql = "update Sandiklar set " +
                    "sandik_adi = ? " +
                    "where id = ?";
            SQLiteStatement stmt = db.compileStatement(sql);
            stmt.bindString(1, yeniSandikAdi);
            stmt.bindLong(2, sandikId);
            stmt.executeUpdateDelete();
            return true;
        } catch(SQLiteConstraintException ex){ // Muhtemelen aynı isimde sandık var.
            mActivity.toast("Aynı isimde sandık zaten var.");
            return false;
        }
    }
    public boolean sandikSil(long sandikId){
        String sql = "delete from Sandiklar " +
                "where id = ?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.bindLong(1, sandikId);
        stmt.executeUpdateDelete();
        return true;
    }
    // ---------------------------------


    // ----------- BİLGİLER -----------
    //Bilgi sorgular
    public Long bilgiIdsiniAl(String bilgi, Long sandikId){
        String sql = "select id from Bilgiler where bilgi=? and sandik_id=?";
        SQLiteStatement stmt = db.compileStatement(sql);
        stmt.bindString(1, bilgi);
        stmt.bindLong(2, sandikId);
        return stmt.simpleQueryForLong();
    }
    public String bilgiAciklamasiniAl(Long sandikId, String bilgi){
        String bilgiAciklamasiQuery = "select aciklama from Bilgiler where " +
                "sandik_id=? and bilgi=?";
        SQLiteStatement stmt = db.compileStatement(bilgiAciklamasiQuery);
        stmt.bindLong(1, sandikId);
        stmt.bindString(2, bilgi);
        return stmt.simpleQueryForString();
    }
    public Cursor tumBilgileriAl(Long sandikId){
        String tablo = "Bilgiler";
        String[] dondurulecekKolonlar = {"id", "bilgi", "aciklama"};
        String selection = "sandik_id = ?";
        String[] selectionArgs =  {sandikId.toString()};
        Cursor cBilgiler = db.query(tablo, dondurulecekKolonlar, selection, selectionArgs,
                null, null, null);
        return cBilgiler;
    }
    //Bilgi ekleme/düzenleme/silme
    public boolean bilgiEkle(Long sandikId, String bilgi, String aciklama){
        try{
            String insertSql = "insert into Bilgiler (sandik_id, bilgi, aciklama, olusturulma_tarihi) values (?, ?, ?, ?)";
            SQLiteStatement insertStmt = db.compileStatement(insertSql);
            insertStmt.bindLong(1, sandikId);
            insertStmt.bindString(2, bilgi);
            insertStmt.bindString(3, aciklama);
            insertStmt.bindString(4, Calendar.getInstance().getTime().toString());
            insertStmt.executeInsert();
            return true;
        } catch(SQLiteConstraintException e){ // amuhtemelen aynı bilgi tekrar eklenmeye çalışılıyor.
            mActivity.toast("Bilgi zaten ekli.");
            return false;
        }
    }
    public boolean bilgiDuzenle(Long sandikId, String bilgi, String yeniBilgi, String yeniAciklama){
        try{
            String sql = "update Bilgiler set " +
                    "bilgi = ?, " +
                    "aciklama = ? where " +
                    "sandik_id =  ? and " +
                    "bilgi = ?";

            SQLiteStatement stmt = db.compileStatement(sql);
            stmt.bindString(1, yeniBilgi);
            stmt.bindString(2, yeniAciklama);
            stmt.bindLong(3, sandikId);
            stmt.bindString(4, bilgi);
            stmt.executeUpdateDelete();
            return true;
        } catch(SQLiteConstraintException ex){ // Muhtemelen aynı isimde bilgi var.
            mActivity.toast("Aynı isimde bilgi zaten var.");
            return false;
        }
    }
    public boolean bilgiDuzenle(Long bilgiId, String yeniBilgi, String yeniAciklama){
        try {
            String sql = "update Bilgiler set " +
                    "bilgi = ?, " +
                    "aciklama = ? " +
                    "where id =  ?";

            SQLiteStatement stmt = db.compileStatement(sql);
            stmt.bindString(1, yeniBilgi);
            stmt.bindString(2, yeniAciklama);
            stmt.bindLong(3, bilgiId);
            stmt.executeUpdateDelete();
        } catch(SQLiteConstraintException ex){ // Muhtemelen aynı isimde bilgi var.
            mActivity.toast("Aynı isimde bilgi zaten var.");
            return false;
        }
        return true;
    }
    // ---------------------------------


    // Sandık tablolarını oluştur/sil
    private void sandiklarTablosunuOlustur(){
        // Sandiklar tablosunu oluştur.
        String sandiklarSql = "create table if not exists Sandiklar (" +
                "id integer primary key autoincrement," +
                "sandik_adi text not null unique," +
                "olusturulma_tarihi text not null" +
                ")";
        SQLiteStatement sandiklarStmt = db.compileStatement(sandiklarSql);
        sandiklarStmt.execute();
    }
    private void bilgilerTablosunuOlustur(){
        // Sandiklar tablosunu oluştur.
        String bilgilerSql = "create table if not exists Bilgiler (" +
                "id integer primary key autoincrement," +
                "sandik_id integer not null," +
                "bilgi text not null," +
                "aciklama text," +
                "olusturulma_tarihi text not null," +
                "foreign key (sandik_id) references Sandiklar(id)," +
                "unique (sandik_id, bilgi)" +
                ")";
        SQLiteStatement bilgilerStmt = db.compileStatement(bilgilerSql);
        bilgilerStmt.execute();
    }
    private void sandiklarTablosunuSil(){
        db.execSQL("drop table if exists Sandiklar");
    }
    private void bilgilerTablosunuSil(){
        db.execSQL("drop table if exists Bilgiler");
    }

    // Örnek sandıklar
    private void ornekSandiklariEkle(){
        sandikEkle("Müzik");
        sandikEkle("Sure İsimleri");
    }
    private void ornekBilgileriEkle(){
        ornekBilgileriEkle("Müzik");
        ornekBilgileriEkle("Sure İsimleri");
    }
    private void ornekBilgileriEkle(String sandikAdi){
        try {
            int bilgilerDosyasi = -1;
            int aciklamalarDosyasi = -1;
            switch (sandikAdi){
                case "Müzik":
                    bilgilerDosyasi = R.raw.muzik_sandigi_bilgiler;
                    aciklamalarDosyasi = R.raw.muzik_sandigi_aciklamalar;
                    break;
                case "Sure İsimleri":
                    bilgilerDosyasi = R.raw.sureisimleri_bilgiler;
                    aciklamalarDosyasi = R.raw.sureisimleri_aciklamalar;
                    break;
            }
            // Ön hazirlik
            Long sandikId = sandikIdsiniAl(sandikAdi);
            // Insert statement oluştur.
            String insertSql = "insert into Bilgiler (sandik_id, bilgi, aciklama, olusturulma_tarihi) values (?, ?, ?, ?)";
            SQLiteStatement insertStmt = db.compileStatement(insertSql);
            Scanner bilgilerScanner = new Scanner(mActivity.getResources().openRawResource(bilgilerDosyasi));
            Scanner aciklamalarScanner = new Scanner(mActivity.getResources().openRawResource(aciklamalarDosyasi));
            while (bilgilerScanner.hasNextLine() && aciklamalarScanner.hasNextLine()){
                String bilgi = bilgilerScanner.nextLine();
                String aciklama = aciklamalarScanner.nextLine();
                insertStmt.bindLong(1,  sandikId);
                insertStmt.bindString(2, bilgi);
                insertStmt.bindString(3, aciklama);
                insertStmt.bindString(4, Calendar.getInstance().getTime().toString());
                insertStmt.executeInsert();
            }
        }
        // Muhtemelen tekrar çağırılmaya çalışılıyor. UNIQUE constraint failed: Bilgiler.sandik_id, Bilgiler.bilgi
        catch (SQLiteConstraintException ex){
            mActivity.toast("Örnek bilgileri zaten eklediniz.");
        }
    }

}

