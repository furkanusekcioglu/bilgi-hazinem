package tk.bilgihazinem.bhandroid.dal;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.transition.Scene;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.google.firebase.firestore.WriteBatch;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tk.bilgihazinem.bhandroid.BilgiDuzenleActivity;
import tk.bilgihazinem.bhandroid.BilgiHazinemActivity;
import tk.bilgihazinem.bhandroid.R;
import tk.bilgihazinem.bhandroid.model.Bilgi;
import tk.bilgihazinem.bhandroid.model.Kullanici;
import tk.bilgihazinem.bhandroid.model.Sandik;

public class BilgiHazinemFirestore {
    private BilgiHazinemActivity mActivity;
    private FirebaseFirestore mFirestore;
    public BilgiHazinemFirestore(BilgiHazinemActivity activity){
        mActivity = activity;
        fireStoreIlkle();
    }
    public void kullaniciEkle(Kullanici kullanici){
        CollectionReference users = mFirestore.collection("kullanicilar");
        users.document(kullanici.email).set(kullanici);
    }
    public void sandikEkle(WriteBatch batch, String email, Sandik sandik){
        DocumentReference sandikRef = mFirestore.collection("kullanicilar")
                .document(email)
                .collection("sandiklar")
                .document(sandik.getSandikAdi());
        batch.set(sandikRef, sandik);
    }

    public void bilgiEkle(WriteBatch batch, String email, String sandikAdi, Bilgi bilgi){
        DocumentReference bilgiRef = mFirestore.collection("kullanicilar")
                .document(email)
                .collection("sandiklar")
                .document(sandikAdi)
                .collection("bilgiler")
                .document(bilgi.getBilgi());

        batch.set(bilgiRef, bilgi);
    }
    public void verileriYedekle(String email){
        BilgiHazinemSqlite sqLite = new BilgiHazinemSqlite(mActivity);
        Cursor cSandiklar = sqLite.tumSandiklariAl();
        WriteBatch sandiklariEkleBatch = mFirestore.batch();
        Map<Long, String> sandiklar = new HashMap<>();
        try {
            while(cSandiklar.moveToNext()){
                String sandikAdi = cSandiklar.getString(cSandiklar.getColumnIndex("sandik_adi"));
                Long sandikId = cSandiklar.getLong(cSandiklar.getColumnIndex("id"));
                sandikEkle(sandiklariEkleBatch, email, new Sandik(sandikId, sandikAdi));
                sandiklar.put(sandikId, sandikAdi);
            }
        } finally {
            cSandiklar.close();
        }
        sandiklariEkleBatch.commit().addOnCompleteListener(sandiklariEkleTask -> {
            if (sandiklariEkleTask.isSuccessful()){
                WriteBatch bilgileriEkleBatch = mFirestore.batch();
                for(Long sandikId : sandiklar.keySet()){
                    Cursor cBilgiler = sqLite.tumBilgileriAl(sandikId);
                    try{
                        while(cBilgiler.moveToNext()){
                            Long bilgiId = cBilgiler.getLong(cBilgiler.getColumnIndex("id"));
                            String bilgi = cBilgiler.getString(cBilgiler.getColumnIndex("bilgi"));
                            String aciklama = cBilgiler.getString(cBilgiler.getColumnIndex("aciklama"));
                            bilgiEkle(bilgileriEkleBatch, email, sandiklar.get(sandikId), new Bilgi(bilgiId, bilgi, aciklama));
                        }
                    } finally {
                        cBilgiler.close();
                    }
                }
                bilgileriEkleBatch.commit().addOnCompleteListener(bilgileriEkleTask -> {
                    if (bilgileriEkleTask.isSuccessful()){
                        mActivity.$pb(R.id.progressBar).setVisibility(View.GONE);
                        mActivity.toast("Yedekleme tamamlandı.");
                    }
                });
            }
        });

//        try{
//            while(cSandiklar.moveToNext()){
//                String sandikAdi = cSandiklar.getString(cSandiklar.getColumnIndex("sandik_adi"));
//                Long sandikId = cSandiklar.getLong(cSandiklar.getColumnIndex("id"));
//                sandikEkle(email, new Sandik(sandikId, sandikAdi));
//                Cursor cBilgiler = sqLite.tumBilgileriAl(sandikId);
//                try{
//                    while(cBilgiler.moveToNext()){
//                        Long bilgiId = cBilgiler.getLong(cBilgiler.getColumnIndex("id"));
//                        String bilgi = cBilgiler.getString(cBilgiler.getColumnIndex("bilgi"));
//                        String aciklama = cBilgiler.getString(cBilgiler.getColumnIndex("aciklama"));
//                        bilgiEkle(email, sandikAdi, new Bilgi(bilgiId, bilgi, aciklama));
//                    }
//                } finally {
//                    cBilgiler.close();
//                }
//            }
//        } finally {
//            cSandiklar.close();
//        }
    }
    private void fireStoreIlkle(){
        mFirestore = FirebaseFirestore.getInstance();
    }
    public void yedeklenmisVerileriAl(String email){
        BilgiHazinemSqlite sqLite = new BilgiHazinemSqlite(mActivity);
        sqLite.tablolariBosalt();

        mFirestore.collection("kullanicilar")
                .document(email)
                .collection("sandiklar")
                .get()
                .addOnCompleteListener(sandiklariAlTask -> {
                    if (sandiklariAlTask.isSuccessful()) {
                        int kalanSandik = sandiklariAlTask.getResult().size();
                        for (QueryDocumentSnapshot document : sandiklariAlTask.getResult()) {
                            String sandikAdi = document.getData().get("sandikAdi").toString();
                            sqLite.sandikEkle(sandikAdi);
                            document.getReference().collection("bilgiler")
                                    .get()
                                    .addOnCompleteListener(bilgileriAlTask -> {
                                        if(bilgileriAlTask.isSuccessful()) {
                                            Long sandikId = sqLite.sandikIdsiniAl(sandikAdi);
                                            for (QueryDocumentSnapshot bilgiDocument : bilgileriAlTask.getResult()) {
                                                Map<String, Object> data = bilgiDocument.getData();
                                                String bilgi = data.get("bilgi").toString();
                                                String aciklama = data.get("aciklama").toString();
                                                sqLite.bilgiEkle(sandikId, bilgi, aciklama);
                                            }
                                        }
                                        else{

                                        }
                                    });
                        }
                    } else {
//                            Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });

    }

}

